#include <iostream>
#include <cstdlib>
#define STB_IMAGE_IMPLEMENTATION
#include "stb/stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb/stb_image_write.h"

int main()
{
    int suma = 0;
    int tab[3][3] = { {1,-2,1},{-2,5,-2},{1,-2,1} }; // TABLICA FILTR WYOSTRZAJACY

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            suma = suma + tab[i][j];
        }
    }
    std::cout << "Dzielona: " << suma << std::endl;
    int w, h, ch;
    std::string path = "color.jpg";
    std::string path2 = "gray.jpg";
    std::string path3 = "gray_filtered.jpg";
    std::string path4 = "color_filtered.jpg";
  
    unsigned char* image = stbi_load(path.c_str(), &w, &h, &ch, 0);
    if (image == NULL) {
        std::cout << "error - brak obrazu";
        exit(1);
    }
    std::cout << "obraz zaladowany" << std::endl;
    std::cout << "szerokosc: " << w << std::endl << "wysokosc: " << h << std::endl << "bajty na piksel: " << ch << std::endl;

    if (suma == 0) {
        std::cout << "error - suma 0";
        exit(1);
    }

    size_t image_size = w * h * ch;
    int gray_ch = ch == 4 ? 2 : 1;
    size_t gray_image_size = w * h * gray_ch;
    unsigned char* gray_image = (unsigned char*)malloc(gray_image_size);
    if (gray_image == NULL) {
        std::cout << "blad alokacji przy czarno bialym";
        exit(1);
    }
    std::cout << "bajty na piksel szary: " << gray_ch << std::endl;
    for (unsigned char* pi = image, *pgi = gray_image; pi != image + image_size; pi += ch, pgi += gray_ch) {
        *pgi = (*pi + *(pi + 1) + *(pi + 2)) / 3;
        if (ch == 4) {
            *(pgi + 1) = *(pi + 3);
        }
    }

    unsigned char* filtr_gray_image = (unsigned char*)malloc(gray_image_size);
    for (unsigned char* pi = gray_image, *pg = filtr_gray_image; pi != gray_image + gray_image_size; pi += gray_ch, pg += gray_ch) {
        if (pi < gray_image + w * gray_ch + gray_ch) {
            *pg = *pi;
            if (gray_ch == 4) {
                *(pg + 1) = *(pi + 1);
            }
        }
        else if (pi < image + image_size - w * gray_ch - gray_ch) {
            *pg = (*(pi - w * gray_ch - gray_ch) * tab[0][0] + *(pi - w * gray_ch) * tab[0][1] + *(pi - w * gray_ch + gray_ch) * tab[0][2]
                + *(pi - gray_ch) * tab[1][0] + *(pi)*tab[1][1] + *(pi + gray_ch) * tab[1][2]
                + *(pi + w * gray_ch - gray_ch) * tab[2][0] + *(pi + w * gray_ch) * tab[2][1] + *(pi + w * gray_ch + gray_ch) * tab[2][2]) / suma;

            if (gray_ch == 4) {
                *(pg + 1) = *(pi + 1);
            }
        }
    }
    else {
        *pg = *pi;
        if (gray_ch == 4) {
            *(pg + 1) = *(pi + 1);
        }
    }

    unsigned char* filtr_colour_image = (unsigned char*)malloc(image_size);
    for (unsigned char* pi = image, *pg = filtr_colour_image; pi != image + image_size; pi += ch, pg += ch) {
        if (pi < image + w * ch + ch) {
            *pg = *pi;
            if (ch == 4) {
                *(pg + 3) = *(pi + 3);
            }
        }
        else if (pi < image + image_size - w * ch - ch) {
            *pg = (*(pi - w * ch - ch) * tab[0][0] + *(pi - w * ch) * tab[0][1] + *(pi - w * ch + ch) * tab[0][2]
                + *(pi - ch) * tab[1][0] + *(pi)*tab[1][1] + *(pi + ch) * tab[1][2]
                + *(pi + w * ch - ch) * tab[2][0] + *(pi + w * ch) * tab[2][1] + *(pi + w * ch + ch) * tab[2][2]) / suma;

            *(pg + 1) = (*((pi + 1) - w * ch - ch) * tab[0][0] + *((pi + 1) - w * ch) * tab[0][1] + *((pi + 1) - w * ch + ch) * tab[0][2]
                + *((pi + 1) - ch) * tab[1][0] + *((pi + 1)) * tab[1][1] + *((pi + 1) + ch) * tab[1][2]
                + *((pi + 1) + w * ch - ch) * tab[2][0] + *((pi + 1) + w * ch) * tab[2][1] + *((pi + 1) + w * ch + ch) * tab[2][2]) / suma;

            *(pg + 2) = (*((pi + 2) - w * ch - ch) * tab[0][0] + *((pi + 2) - w * ch) * tab[0][1] + *((pi + 2) - w * ch + ch) * tab[0][2]
                + *((pi + 2) - ch) * tab[1][0] + *((pi + 2)) * tab[1][1] + *((pi + 2) + ch) * tab[1][2]
                + *((pi + 2) + w * ch - ch) * tab[2][0] + *((pi + 2) + w * ch) * tab[2][1] + *((pi + 2) + w * ch + ch) * tab[2][2]) / suma;

            if (ch == 4) {
                *(pg + 3) = *(pi + 3);
            }
        }
    }
    else {
        *pg = *pi;
        if (ch == 4) {
            *(pg + 3) = *(pi + 3);
        }
    }

    stbi_write_jpg(path.c_str(), w, h, ch, image, 80);
    stbi_write_jpg(path4.c_str(), w, h, ch, filtr_colour_image, 80);
    stbi_write_jpg(path2.c_str(), w, h, gray_ch, gray_image, 80);
    stbi_write_jpg(path3.c_str(), w, h, gray_ch, filtr_gray_image, 80);

    stbi_image_free(image);
    stbi_image_free(filtr_colour_image);
    stbi_image_free(gray_image);
    stbi_image_free(filtr_gray_image);
}